package com.grassknight.easymock.demo;

public interface Collaborator {
    void documentAdded(String title);
}
