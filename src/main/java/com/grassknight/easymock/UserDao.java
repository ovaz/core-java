package com.grassknight.easymock;

public class UserDao {
    public User getUserById(String id) {
        User user = new User();
        user.setName("罗志祥");
        user.setAge(40);
        user.setSex("男");
        user.setJob("程序员");
        return user;
    }
}
