package com.grassknight.easymock;

public class EasyMockTest {
    private UserDao userDao = new UserDao();

    public String getUserName() {
        User user = userDao.getUserById("19041393");
        if (user != null) {
            return user.getName();
        }
        return "this is a good question";
    }
}
