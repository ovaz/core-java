package com.grassknight.jdk1_8.lambda;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class LambdaTest1 {
    //这是slf4j的接口，由于我们引入了logback-classic依赖，所以底层实现是logback
    private static final Logger LOGGER = LoggerFactory.getLogger(LambdaTest1.class);

    @BeforeMethod
    public void beforeMethod() {
        LOGGER.debug("##########################华丽的分割线##########################");
    }

    @Test
    public void test1() {

        //匿名内部类
        Comparator<Integer> cpt = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return Integer.compare(o1, o2);
            }
        };
        TreeSet<Integer> set = new TreeSet<>(cpt);
        set.add(100);
        set.add(5);
        set.add(20);
        for (Integer integer : set) {
            LOGGER.debug(integer.toString());
        }
        LOGGER.debug("=========================");
        //使用lambda表达式
        Comparator<Integer> cpt2 = (x, y) -> Integer.compare(x, y);
        Comparator<Integer> cpt3 = Integer::compare;
        TreeSet<Integer> set2 = new TreeSet<>(cpt2);
        set2.add(120);
        set2.add(54);
        set2.add(2321);
        for (Integer integer : set2) {
            LOGGER.debug(integer.toString());
        }
    }


    @Test
    public void LambdaTest2() {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product("蓝色", 100));
        productList.add(new Product("黄色", 123));
        productList.add(new Product("绿色", 1000));
        productList.add(new Product("黑色", 900));
        productList.add(new Product("红色", 900));
        List<Product> resultList = filterProductByColor(productList);
        for (Product product : resultList) {
            LOGGER.debug(product.toString());
        }
        List<Product> resultList2 = filterProductByPrice(productList);
        for (Product product : resultList2) {
            LOGGER.debug(product.toString());
        }

    }

    @Test
    public void test3() {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product("蓝色", 100));
        productList.add(new Product("黄色", 123));
        productList.add(new Product("绿色", 1000));
        productList.add(new Product("黑色", 900));
        productList.add(new Product("红色", 900));
        MyPredicate<Product> myPredicate = new PricePredicate();

        List<Product> resultList = filterProductByPredicate(productList, myPredicate);
        for (Product product : resultList) {
            LOGGER.debug(product.toString());
        }
    }


    // 筛选颜色为红色
    public List<Product> filterProductByColor(List<Product> list) {
        List<Product> prods = new ArrayList<>();
        for (Product product : list) {
            if ("红色".equals(product.getColor())) {
                prods.add(product);
            }
        }
        return prods;
    }

    // 筛选价格小于8千的
    public List<Product> filterProductByPrice(List<Product> list) {
        List<Product> prods = new ArrayList<>();
        for (Product product : list) {
            if (product.getPrice() < 8000) {
                prods.add(product);
            }
        }
        return prods;
    }

    /**
     * 定义过滤方法，将过滤接口当做参数传入，这样这个过滤方法就不用修改，在实际调用的时候将具体的实现类传入即可。
     *
     * @param list
     * @param mp
     * @return
     */
    public List<Product> filterProductByPredicate(List<Product> list, MyPredicate<Product> mp) {
        List<Product> prods = new ArrayList<>();
        for (Product prod : list) {
            if (mp.test(prod)) {
                prods.add(prod);
            }
        }
        return prods;
    }
}
