package com.grassknight.jdk1_8.lambda;

public interface MyPredicate <T> {
    boolean test(T t);
}