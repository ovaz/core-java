package com.grassknight.jdk1_8.lambda;

public class Product {
    private String color;
    private Integer price;

    public Product(String color, Integer price) {
        this.color = color;
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "color='" + color + '\'' +
                ", price=" + price +
                '}';
    }
}
