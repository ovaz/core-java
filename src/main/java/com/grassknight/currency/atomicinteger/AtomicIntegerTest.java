package com.grassknight.currency.atomicinteger;

import org.testng.annotations.Test;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerTest {
    @Test
    public void test1() {
        AtomicInteger atomicInteger = new AtomicInteger();
        atomicInteger.incrementAndGet();


        atomicInteger.incrementAndGet();
//        atomicInteger.accumulateAndGet();
        System.out.println(atomicInteger);


        // todo method
        // 减1，返回修改后的值
        atomicInteger.decrementAndGet();
        // 减1，返回修改前的值
        atomicInteger.getAndDecrement();

        // 加1，返回修改后的值
        atomicInteger.incrementAndGet();
        // 加1，返回修改前的值
        atomicInteger.getAndIncrement();

        // 增加指定值，返回修改后的值
        atomicInteger.addAndGet(2);
        // 增加指定值，返回修改前的值
        atomicInteger.getAndAdd(2);

        // 设置为指定值，返回旧值
        atomicInteger.getAndSet(1);

        // 设置为指定值
        atomicInteger.set(5);

        // CAS比较更新
        atomicInteger.compareAndSet(4,5);

        // 获取当前值
        atomicInteger.get();

        // 获取当前值，转为double
        atomicInteger.doubleValue();
        // 获取当前值，转为float
        atomicInteger.floatValue();
        // 获取当前值，转为int
        atomicInteger.intValue();
        // 获取当前值，转为long
        atomicInteger.longValue();
        // 获取当前值，转为String
        atomicInteger.toString();

        // 普通的设置，不保证可见性和原子性？
        atomicInteger.lazySet(10);
        atomicInteger.weakCompareAndSet(4,5);

    }
}
