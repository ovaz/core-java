package com.grassknight.currency;

public class DeadLockDemo {
    private static Object LockObjectA = new Object();
    private static Object LockObjectB = new Object();

    public static void main(String[] args) {

        Thread threada = new Thread(() -> {
            synchronized (LockObjectA) {
                System.out.println(Thread.currentThread().getName() + "获取锁A");
                synchronized (LockObjectB) {
                    System.out.println(Thread.currentThread().getName() + "获取锁B");
                }
            }
        });

        Thread threadb = new Thread(() -> {
            synchronized (LockObjectB) {
                System.out.println(Thread.currentThread().getName() + "获取锁B");
                synchronized (LockObjectA) {
                    System.out.println(Thread.currentThread().getName() + "获取锁A");
                }
            }
        });

        threada.start();
        threadb.start();
    }
}
